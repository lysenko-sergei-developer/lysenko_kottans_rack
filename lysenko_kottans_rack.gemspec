lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require './lib/lysenko_kottans_rack/version'

Gem::Specification.new do |s|
  s.name          = 'lysenko_kottans_rack'
  s.version       = LysenkoKottansRack::VERSION
  s.date          = '2016-11-27'
  s.authors       = ['Sergey Lysenko']
  s.summary       = 'lysenko_kottans_rack'
  s.description   = 'Gem contains hw3'

  s.authors       = ['Sergey Lysenko']
  s.email         = 'yamaha001001@gmail.com'
  s.files         = 'git ls-files -z'.split("\x0").reject do |f| f =~ %r{^(test|s|features)/}
  s.homepage      = 'https://github.com/lysenko-sergei-developer'
  s.license       = 'MIT'

  end

  s.bindir        = 'exe'
  s.executables   = s.files.grep(%r{^exe/}) { |f| File.basename(f) }
  s.require_paths = ['lib']
end
